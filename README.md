# blepd

These functions evaluate  whether a change in a/some terminal branch length generates a change in the area(s) selected using PD; and when applies, the terminal branch length value for that change.